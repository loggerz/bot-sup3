const fs = require('fs');
const Discord = require('discord.js');

const messages = {
	helpPoll1: fs.readFileSync('messages/poll-help-1.txt'),
	helpPoll2: fs.readFileSync('messages/poll-help-2.txt'),
	helpPoll3: fs.readFileSync('messages/poll-help-3.txt'),
	helpAdmin: fs.readFileSync('messages/admin-help.txt'),
	helpGlobal: fs.readFileSync('messages/global-help.txt'),
};

function privateResponseError(message, error) {
	const user = message.author;
	const errorEmbed = new Discord.MessageEmbed()
		.setColor('#ff4f29')
		.setTitle('Erreur')
		.setDescription(error)
		.setTimestamp()
		.setFooter('Si vous pensez que ce problème ne vient pas de vous, contactez Barbézout');
	user.send(errorEmbed).catch(err => {});
}

let numbers = ['0️⃣', '1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣'];
let emojis = {
	'question': '❓'
};


function afterReady() {	
}

function getNumber(i) {
	return numbers[i]
}

function getEmoji(name) {
	return emojis[name];
}

function isAdmin(message) {
	const adminRole = message.guild.roles.cache.find(role => role.name === process.env.ADMIN_ROLE);
	return adminRole.members.has(message.author.id);
}

module.exports = { messages, privateResponseError, getNumber, getEmoji, afterReady, isAdmin }