/*
//
//	POLL COMMAND MODULES - BOT SUP3
//
*/

// function displayHelp(message) {

// }

const fs = require('fs');
const Discord = require('discord.js');
const { privateResponseError, messages, getNumber, getEmoji } = require('../utilities');

/*
Poll structure :
{
	title: string,
	options: [
		"Option 1", ..., "Option 10"
	]
	messageId: Snowflake
	pollNo: integer
}
*/
// let polls = {};



function privateResponseHelp(message) {
	const user = message.author;
	const helpEmbed = new Discord.MessageEmbed()
		.setColor('#0099ff')
		.setTitle('Commandes pour les @admins seulement')
		.addFields(
			{ name: 'Commandes globales', value: messages.helpAdmin },
		);
	user.send(helpEmbed);
}

module.exports = {
	name: 'admin',
	description: 'Admin only cmds',

	execute(message) {
		const adminRole = message.guild.roles.cache.find(role => role.name === process.env.ADMIN_ROLE);
		if (!adminRole.members.has(message.author.id)) {
			console.error(message.author.username + ' tried to use an admin cmd !!');
			privateResponseError(message, 'Vous devez être dans @' + process.env.ADMIN_ROLE + ' pour utiliser cette commande');
			message.delete();
			return;
		};

		const args = message.content.split(' ');
		args.shift();
		console.log('Admin cmd args by ' + message.author.username + ' : ', args.join(' '));

		if (args.length === 0) {
			privateResponseHelp(message);
			message.delete();
		}

		else if (args[0] === 'help') {
			privateResponseHelp(message);
			message.delete();
		}

		else if (args[0] === 'say') {
			args.shift();
			message.delete();
			message.channel.send(args.join(' '));
		}
		
		else if (args[0] === 'delete') {
			if (isNaN(parseInt(args[1]))) {
				privateResponseError(message, 'Le dernier argument doit être un nombre. Cf `admin help`');
				return;
			}
			message.channel.bulkDelete(parseInt(args[1]) + 1).catch(err => {
				privateResponseError(message, 'bulkDelete a retourné une erreur : ' + err);
				message.delete();
			});
		} else {
			privateResponseError(message, 'Commande inconnue "' + args[0] + '"');
			message.delete();
		}
	},
};