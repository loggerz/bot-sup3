/*
//
//	POLL COMMAND MODULES - BOT SUP3
//
*/

const fs = require('fs');
const Discord = require('discord.js');
const { privateResponseError, messages, getNumber, getEmoji, isAdmin } = require('../utilities');


const watchList = process.env.ATTACH_ONLY_CHANNELS.split(',');

module.exports = {
	name: 'attachmentsOnly',
	description: 'Watcher attachmentsOnly',

	execute(message) {
        // Only in the ATTACH_ONLY_CHANNELS channels
        if (!watchList.includes(message.channel.name)) return;

        // Only non-admins
        const adminRole = message.guild.roles.cache.find(role => role.name === process.env.ADMIN_ROLE);
        if (adminRole.members.has(message.author.id)) return;

        if (message.attachments.size === 0) {
            privateResponseError(message, 'Ce channel est destiné à des messages contenant des images ou autre pièces-jointes. **Vous ne pouvez pas discuter ici**');
            message.delete();
        }
	}
};