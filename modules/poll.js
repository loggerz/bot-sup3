/*
//
//	POLL COMMAND MODULES - BOT SUP3
//
*/

const fs = require('fs');
const Discord = require('discord.js');
const { privateResponseError, messages, getNumber, getEmoji, isAdmin } = require('../utilities');

// Poll-specific help private msg
function privateResponseHelp(message) {
	const user = message.author;
	const helpEmbed = new Discord.MessageEmbed()
		.setColor('#0099ff')
		.setTitle('Commandes pour les sondages')
		.addFields(
			{ name: 'Commandes globales', value: messages.helpPoll1 },
			{ name: 'Commandes d\'un sondage', value: messages.helpPoll2 },
			{ name: '.. pour le créateur du sondage', value: messages.helpPoll3 },
		);
	user.send(helpEmbed);
}

function genPollStructure(username, title, id) {
	let str = ':bar_chart: *' + username + '* : **' + title + '**\u000D';
	str += 'n°' + id + ' - Options imposées';
	return str;
}

function createPoll(message, pollTitle, username) {
	message.channel.send(genPollStructure(username, pollTitle, pollNextId)).then(post => {
		const poll = {
			id: pollNextId,
			messageId: post.id,
			free: false,
			rows: [],
			authorId: message.author.id
		}
		polls.push(poll);
		pollNextId++;
		console.log('Polls count : ' + polls.length);
		// post.react(getEmoji('question'));
	}).catch(err => {
		privateResponseError(message, err);
	});
}

/* ---- poll.js data, saved in data/polls.json file ---- */

let pollNextId = -1;
let polls = [];
/* polls structure :
polls = [
	{
		id: int,
		messageId: Snowflake,
		free: false,
		rows: [
			"option 1", "option 2" ..., "option 10" 
		],
		authorId: int
	}
]
*/

/* ---- END OF poll.js DATA ---- */

module.exports = {
	name: 'poll',
	description: 'Poll creation',

	poll(message) {
		/* ---- CALLED FOR EVERY MSG STARTING WITH `poll` OR IN THE #POLLS_CHANNEL ---- */
		
		// Prevent `poll` cmds in other channels
		if (message.channel.name !== process.env.POLLS_CHANNEL) {
			if (message.channel.type === 'dm') {
				message.channel.send('Voici l\'aide des sondages mais pour qu\'une commande fonctionne il faut être dans le channel #sondages');
				privateResponseHelp(message);
				return;
			}

			message.reply('Hé oh il faut être dans le channel #' + process.env.POLLS_CHANNEL + ' pour utiliser les sondages').then(rep => {
				rep.delete({ timeout: 5000 });
				message.delete({ timeout: 5000 }).catch(() => {}); // Silent the error when in DMChannel
			});
			return;
		}

		const args = message.content.split(' ');
		args.shift();
		console.log('Poll cmd args by ' + message.author.username + ' : ', args);

		// Action distribution ...
		if (args.length === 0) {
			privateResponseHelp(message);
			message.delete();
		}
		else if (args[0] === 'help') {
			privateResponseHelp(message);
			message.delete();
		}
		else if (args[0] === 'new') {
			args.shift();
			if (args.length === 0) {
				privateResponseError(message, 'Vous n\'avez pas donné de nom à votre sondage !');
				message.delete();
				return;
			}
			const pollTitle = args.join(' ');
			if (pollTitle.indexOf('~') !== -1 || pollTitle.indexOf('*') !== -1 || pollTitle.indexOf('_') !== -1) {
				privateResponseError(message, 'Les caractères suivants  `~`,`*`,`_` ne sont pas autorisés pour le titre des sondages');
				message.delete();
				return;
			}
			const guild = message.guild;
			const member = guild.member(message.author);
			const nickname = member ? member.displayName : null;
			createPoll(message, pollTitle, nickname ? nickname : message.author.username);
			message.delete();
		} else if (args.length === 1) {
			privateResponseError(message, 'Commande inconnue "' + args[0] + '"');
			message.delete();
		} else {
			const id = parseInt(args[0]);
			if (isNaN(id)) {
				privateResponseError(message, 'Le premier argument après poll doit être un entier, le numéro du sondage en l\'occurence');
				message.delete();
				return;
			} else if (polls.findIndex(poll => poll.id === id) === -1) {
				privateResponseError(message, 'Le sondage n°' + id + ' est introuvable');
				message.delete();
				return;
			}

			args.shift();
			const pollSelected = polls.find(poll => poll.id === id)
			const messageIdSelected = pollSelected.messageId;

			message.channel.messages.fetch(messageIdSelected).then(messageSelected => {

				// Check local permissions
				const isOwner = (isAdmin(message) || pollSelected.authorId === message.author.id)
				const canEdit = (isOwner || pollSelected.free);

				function mustBeOwnerErr() {
					privateResponseError(message, 'Vous ne pouvez effectuer cette action sur ce sondage, cela est reservé au propriétaire du sondage ou aux admins');
					message.delete();
				}

				if (args[0] === 'delete') {
					if (!isOwner) return mustBeOwnerErr();
					message.delete();
					messageSelected.delete();
					polls.splice(polls.findIndex(p => p.id === id), 1);
					this.savePolls();
				} else if (args[0] === 'free' || args[0] === 'restricted') {
					if (!isOwner) return mustBeOwnerErr();
					pollSelected.free = (args[0] === 'free');
					// msg gui update
					const content = messageSelected.content.split('\r');
					const secondLine = content[1].split('Options ');
					secondLine[1] = pollSelected.free ? 'libres' : 'imposées';
					content[1] = secondLine.join('Options ');
					messageSelected.edit(content.join('\r'));
					message.delete();
				} else if (args[0] === 'remove') {
					if (!isOwner) return mustBeOwnerErr();
					if (args[1] === 'all') {
						pollSelected.rows = [];
						const oldContent = messageSelected.content.split('\r');
						const newContent = [oldContent[0], oldContent[1]];
						messageSelected.edit(newContent.join('\r'));
						messageSelected.reactions.removeAll(); // .then(messageSelected.react(getEmoji('question')));
						message.delete();
						return;
					}


					// PLACEHOLDER
					privateResponseError(message, 'Cette fonctionnalité n\'est pas encore implémentée. Seul `all` est possible pour cette commande');
					message.delete();
					return;


					const rowNo = parseInt(args[1]);
					if (isNaN(rowNo) || rowNo <= 0 || rowNo > pollSelected.rows.length) {
						privateResponseError(message, 'Le paramètre suivant `remove` doit être `all` ou un entier entre 1 et ' + pollSelected.rows.length + ' représentant le numéro de ligne à retirer');
						message.delete();
						return;
					}


				} else if (args.length >= 1) {
					if (!canEdit) {
						privateResponseError(message, 'Vous ne pouvez pas ajouter de champ sur ce sondage, cela est reservé au propriétaire du sondage ou aux admins. Ils peuvent cependent donner l\'autorisation avec `poll [n°] free`');
						message.delete();
						return;
					}
					// Add
					
					if (pollSelected.rows.length >= 10) {
						privateResponseError(message, 'La limite de 10 options maximales pour le sondage est atteinte, vous devez supprimer une option pour en mettre une nouvelle');
						message.delete();
						return;
					}

					const newOption = args.join(' ');
					// Prevent formatting
					if (newOption.indexOf('~') !== -1 || newOption.indexOf('*') !== -1 || newOption.indexOf('_') !== -1) {
						privateResponseError(message, 'Les caractères suivants  `~`,`*`,`_` ne sont pas autorisés pour les options');
						message.delete();
						return;
					}

					const content = messageSelected.content.split('\r');
					pollSelected.rows.push(newOption);
					content.push(getNumber(pollSelected.rows.length % 10) + ' ' + newOption);
					messageSelected.react(getNumber(pollSelected.rows.length % 10));
					messageSelected.edit(content.join('\r'));
					message.delete();
				} else {
					privateResponseError(message, 'Vous n\'avez pas spécifié de commande pour le sondage n°' + id);
					message.delete();
				}
			}).catch(err => {
				privateResponseError(message, err);
				message.delete();
			});
		}
	},

	otherMessage(message) {
		const adminRole = message.guild.roles.cache.find(role => role.name === process.env.ADMIN_ROLE);
		if (message.author.bot === true || adminRole.members.has(message.author.id)) return;
		// Not a bot/@admins ? Delete
		message.delete();
	},

	savePolls() {
		fs.writeFileSync('data/polls.json', JSON.stringify({ polls, pollNextId }));
		console.log('Polls saved');
	},

	loadPolls() {
		let data;
		try {
			const fileData = fs.readFileSync('data/polls.json');
			data = JSON.parse(fileData);
		}
		catch (err) {
			data = { polls: [], pollNextId: 0 };
		}
		polls = data.polls;
		pollNextId = data.pollNextId;
		console.log('Polls loaded, polls count, pollNextId : ', polls.length, pollNextId);
	}
};