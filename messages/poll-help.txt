**Commandes "poll" pour les sondages - Bot SUP3**
:information_source: Certains résultats de commande sont envoyés par message privé, pas sur le salon


- Commandes globales

`poll` | affiche l'aide
`poll help` | affiche l'aide
`poll new [titre]` | crée un nouveau sondage


- Commandes pour un poll spécifique

`poll [n°] bump` | fait remonter le sondage
`poll [n°] [texte]` | ajoute une nouvelle option (si possible, *max 10*)


- ... pour le créateur du sondage

`poll [n°] end` | termine le sondage
`poll [n°] free` | tout le monde peut ajouter des options
`poll [n°] restricted` | inverse `poll [...] free`
`poll [n°] remove [n° option]` | enlève une option
`poll [n°] remove all` | enlève toutes les options
`poll [n°] purge` | enlève les options à 0 votes
`poll [n°] delete` | supprime le sondage


Cliquer sur :x: pour supprimer ce message