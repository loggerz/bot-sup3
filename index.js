if(!process.env.TOKEN1) require('dotenv').config();
const fs = require('fs');
const Discord = require('discord.js');

const client = new Discord.Client();
const clientLonni = new Discord.Client();
client.modules = new Discord.Collection();

const { privateResponseError, afterReady, messages } = require('./utilities');

function privateResponseGlobalHelp(message) {
	//const user = message.author;
	const helpEmbed = new Discord.MessageEmbed()
		.setColor('#0099ff')
		.setTitle('Commandes pour les deux bots SUP3')
		.addFields(
			{ name: 'AHOU AHOU AHOU !' , value: messages.helpGlobal },
		);
	message.reply(helpEmbed);
}

// Env vars check
if (!process.env.TOKEN1) {
	console.error('Missing TOKEN env variable. Exiting');
	return process.exit(1);
}
// Command-specific files parsing
const moduleFiles = fs.readdirSync('./modules').filter(file => file.endsWith('.js'));
for (const file of moduleFiles) {
	const module = require(`./modules/${file}`);
	client.modules.set(module.name, module);
}

client.once('ready', () => {
	afterReady();
	console.log('Ready!');
});

clientLonni.once('ready', () => {
	console.log('Lonni ready');
});

client.on('error', err => {
	console.error('Client error : ', err);
});

clientLonni.on('error', err => {
	console.error('Client lonni error : ', err);
});

client.on('message', message => {
	try {
		const args = message.content.split(/ +/);
		args.shift();

		if (message.content.startsWith(process.env.ADMIN_PREFIX)) {
			client.modules.get('admin').execute(message);
		} else if (message.content.startsWith(process.env.POLLS_PREFIX)) {
			// If starts with 'poll'
			client.modules.get('poll').poll(message);
		} else if (message.channel.name === process.env.POLLS_CHANNEL) {
			// If doesn't start with 'poll' but is sent in POLLS_CHANNEL (has to be deleted)
			client.modules.get('poll').otherMessage(message);
		} else if (message.channel.name === process.env.ROLES_CHANNEL) {
			// Auto delete role msgs after 5s
			const adminRole = message.guild.roles.cache.find(role => role.name === process.env.ADMIN_ROLE);
			if (message.author.bot === true
				|| (message.content.startsWith('-role'))) return message.delete({ timeout: 5000 });
			else if (adminRole.members.has(message.author.id)) return;
			// Not a bot, or an admin with -role msg
			privateResponseError(message, 'Vous ne pouvez pas écrire dans ce channel. Il est dédié aux commandes `-role`');
			message.delete();

		} else if (message.channel.type === 'dm') {
			if (message.author.bot) return;
			privateResponseGlobalHelp(message);
			return;
		} else {
			// EVERY other messages
			client.modules.get('attachmentsOnly').execute(message);

		}
	} catch (error) {
		console.error(error);
	}
});

clientLonni.on('message', message => {
	try {
		const args = message.content.split(/ +/);
		args.shift();

		if (message.author.bot) return;

		if (message.content.startsWith('lonnisay')) {
			if (args.length === 0) {
				privateResponseError(message, 'Aucun message à envoyer ... Pour rappel : `lonnisay [votre texte]`');
				message.delete();
				return;
			}
			console.log('Lonni Say by ' + message.author.id + ' : ' + args.join(' '));
			message.delete();
			message.channel.send('Je sais pas si c\'est une blague mais <@!' + message.author.id + '> m\'a dit de dire ' + args.join(' '));
		} else if (message.content.startsWith('ladmin say')) {
			message.delete();
			args.shift();
			message.channel.send(args.join(' '));
		} else if (message.channel.type === 'dm') {
			privateResponseGlobalHelp(message);
			return;
		}
	} catch (error) {
		console.error(error);
	}
});

// ----- Init ----- //
console.log('--- BOT SUP3 #niklasup4 ---');
console.log('Init ...');

client.modules.get('poll').loadPolls();
client.login(process.env.TOKEN1);

clientLonni.login(process.env.TOKEN2)

// ----- EXIT ----- //
let exiting = false;
function safeExit() {
	if (exiting) return;
	exiting = true;
	// Save polls
	client.modules.get('poll').savePolls();

	console.log('Ready to exit safely');
	process.exit(0);
}
process.on('exit', safeExit);
process.on('SIGINT', safeExit);
process.on('SIGUSR1', safeExit);
process.on('SIGUSR2', safeExit);